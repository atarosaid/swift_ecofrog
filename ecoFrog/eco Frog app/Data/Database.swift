//
//  Database.swift
//  eco Frog app
//
//  Created by Said Ataro on 08/12/2020.
//

import SwiftUI
import MapKit

//CATEGORIES
var maison = GameCategory(title: "House",
                          image:"maison",
                          games: [Game(title: "Salon", imageName: "salon"),
                                  Game(title: "Salle de bain", imageName: "salle_de_bain"),
                                  Game(title: "Cuisine", imageName: "cuisine"),
                                  Game(title: "Jardin", imageName: "salon")])

var plage =  GameCategory(title: "Beach",
                          image: "plage",
                          games: [Game(title: "Digue", imageName: "digue"),
                                  Game(title: "Parking", imageName: "plage_parking"),
                                  Game(title: "Phare", imageName: "phare"),
                                  Game(title: "Sable", imageName: "sable")])

var montagne = GameCategory(title: "Mountain",
                            image: "montagne",
                            games: [Game(title: "Ski", imageName: "ski"),
                                    Game(title: "Forêt", imageName: "foret"),
                                    Game(title: "Chalet", imageName: "chalet"),
                                    Game(title: "Patinoire", imageName: "patinoire")])

var voiture = GameCategory(title: "Car",
                           image: "voiture",
                           games:
                            [Game(title: "Coffre", imageName: "coffre"),
                             Game(title: "Gas station", imageName: "gas-station"),
                             Game(title: "Parking", imageName: "parking"),
                             Game(title: "Station de lavage", imageName: "lavage")])


var supermarché = GameCategory(title: "Supermarket",
                                image: "supermarché",
                                games:[Game(title: "Parking", imageName: "parking_supermarche"),
                                       Game(title: "Rayon", imageName: "rayon"),
                                       Game(title: "Panier de courses", imageName: "panier_de_courses"),
                                       Game(title: "Caisse", imageName: "caisses")])

var parc = GameCategory(title:"Park",
                        image:"parc",
                        games: [Game(title: "Bac a sable", imageName: "bac-sable"),
                                Game(title: "Aire de jeux", imageName: "air"),
                                Game(title: "Manège", imageName: "manege"),
                                Game(title: "Zoo", imageName: "zoo")])

var categories = [maison, plage, montagne, voiture, supermarché, parc]

//RECOMPENSES (PRIZES)
var recompense01 = Prize(text: "coton", image: "prize01")
var recompense02 = Prize(text: "étain", image: "prize02")
var recompense03 = Prize(text: "porcelaine", image: "prize03")
var recompense04 = Prize(text: "perle", image: "prize04")
var recompense05 = Prize(text: "émeraude", image: "prize05")
var recompense06 = Prize(text: "argent", image: "prize06")
var recompense07 = Prize(text: "or", image: "prize07")
var recompense08 = Prize(text: "diamant", image: "prize08")
var recompense09 = Prize(text: "platine", image: "prize09")
var recompense10 = Prize(text: "cryptonic", image: "prize10")

var récompenses = [recompense01, recompense02, recompense03,recompense04,recompense05,recompense06,recompense07,recompense08,recompense09,recompense10]

//FRIENDS
var friend01 = Friend(name: "Ayané",
                      image: "ayane",
                      online: false,
                      coordinate: CLLocationCoordinate2D(latitude: 50.64117399037568, longitude:3.044964825493186),
                      activity: activity01)

var friend02 = Friend(name: "Cindy-Rose",
                      image: "cindy",
                      online: true,
                      coordinate: CLLocationCoordinate2D(latitude: 50.059170, longitude:1.382472),
                      activity: activity02)

var friend03 = Friend(name: "Rémy",
                      image: "remy",
                      online: true,
                      coordinate: CLLocationCoordinate2D(latitude: 42.9355873, longitude:0.0805169),
                      activity: activity03)

var friend04 = Friend(name: "Saïd",
                      image: "said",
                      online: true,
                      coordinate: CLLocationCoordinate2D(latitude: 50.636302130881745, longitude:3.147696642699856),
                      activity: activity04)

var friend05 = Friend(name: "Manu",
                      image: "myAvatar",
                      online: true,
                      coordinate: CLLocationCoordinate2D(latitude: 48.774620078430125, longitude:2.300157028506643),
                      activity: activity05)

var friend06 = Friend(name: "Rachid",
                      image: "rachid",
                      online: false,
                      coordinate: CLLocationCoordinate2D(latitude: 44.6008723, longitude:6.3226072),
                      activity: activity06)

var friend07 = Friend(name: "Seng",
                      image: "seng",
                      online: false,
                      coordinate: CLLocationCoordinate2D(latitude: 50.618572, longitude:3.130546),
                      activity: activity07)

var friend08 = Friend(name: "Sabine",
                      image: "sabine",
                      online: false,
                      coordinate: CLLocationCoordinate2D(latitude: 50.604125442061765, longitude:3.0755379581950506),
                      activity: activity08)

var friends = [friend01,friend02, friend03, friend04, friend05, friend06, friend07, friend08]


//Avtivities details

var activity01 = Activity(title: "La ballade à la découverte de la biodiversité autour de la Deûle",
                          description: """
                                        \" Nuit de la chouette\"
                                        rdv citadelle de lille à 20 H. le 15 mars 2021
                                        """)

var activity02 = Activity(title: "Ocean cleanup",
                          description: """
                                        Nettoyage des dechets sur la plage
                                        le 31 decembre 2020 à 10 H. ,Espl. Louis Aragon, 76470 Le Tréport
                                        """)

var activity03 = Activity(title: "Aidez les grenouilles et crapauds à traverser",
                          description: """
                                        La préservations de la biodiversité en dépends
                                        grande soirée aide ta grenouille à traverser
                                        le vendredi 9 et le samedi 10 mars 2021, entre 18H30 et 23 H., 6 Boulevard Gabriel, 21000 Dijon
                                        """)

var activity04 = Activity(title: "Un atelier mobilier en carton",
                          description: """
                                        Construis ton meuble avec des cartons (recyclage)
                                        le 27 fevrier 2021 à 10 H.
                                        Atelier Val cartonne 65 Rue du Petit Pont, 59650 Villeneuve-d'Ascq
                                        """)

var activity05 = Activity(title: "L'opération Tchaomégots",
                          description: """
                                        Ramassage de mégots afin de transformer les mégots en materiaux d'isolation en vu de les transformer en doudounes pour les longues promenades dans le froid
                                        rdv Place du Tertre, 75018 Paris 14 fevrier 2021 à 13 H.
                                        """)

var activity06 = Activity(title: "La fête de l'environnement",
                          description: """
                                        Atelier, stand, concerts gratuits
                                        rendez vous Rue de la Mairie, 13860 Peyrolles-en-Provence le 26 septembre 2021
                                        """)


var activity07 = Activity(title: "La fête de l'environnement en milieu urbain",
                          description: """
                                        Exposition photo au coeur des parcs urbains
                                        rdv  Boulevard Jean-Baptiste Lebas, 59000 Lille
                                        le 24 avril 2021, Des 8 H.
                                        """)

var activity08 = Activity(title: "Nettoyer ton kilomètre",
                          description: """
                                        11H30  le samedi 9 janvier 2021
                                        rendez vous Colonne de la Déesse à lille
                                        """)

var activities = [activity01, activity02, activity03, activity04, activity05, activity06, activity07, activity08]

//Setting

var setting01 = SettingCategory(title:"Notification", modalPage: "NotificationDetail", numberModal: 1)
var setting02 = SettingCategory(title:"Profile setting", modalPage: "ProfilSettingDetail", numberModal: 2)
var setting03 = SettingCategory(title:"Parental control", modalPage: "ParentalControlDetail", numberModal: 3)
var setting04 = SettingCategory(title:"Language", modalPage: "LanguageDetail", numberModal: 4)
var setting05 = SettingCategory(title:"Connection", modalPage: "ConnectionDetail", numberModal: 5)
var setting06 = SettingCategory(title:"About", modalPage: "AboutDetail", numberModal: 6)

var settings = [setting01,setting02, setting03, setting04, setting05, setting06]
//var settings = [, "Notification", "Code parental", "Langue", "Se connecter", "A propos"]
//Obstacles

var obstacle01 = Obstacles(image: "obstacle01")
var obstacle02 = Obstacles(image: "obstacle02")
var obstacle03 = Obstacles(image: "obstacle03")
var obstacle04 = Obstacles(image: "obstacle03")
var obstacle05 = Obstacles(image: "obstacle03")

var obstacles = [obstacle01, obstacle02, obstacle03, obstacle04, obstacle05]
