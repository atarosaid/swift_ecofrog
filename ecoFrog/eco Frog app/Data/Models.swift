//
//  Models.swift
//  test
//
//  Created by Ayane Shoji on 08/12/2020.
//

import SwiftUI
import MapKit

enum Category {
    enum maison {
        case salle_de_bain, cuisine, salon, jardin, chambre
    }
    
    enum plage {
        case digue, parking, phare, camping
    }
    
    enum montagne {
        case parc, forêt, chalet
    }
    
    enum voiture {
        case coffre, route, aire_de_repos, station_de_lavage
    }
    
    enum supermarché {
        case parking, rayon, chariot, recyclage
    }
    
    enum parc {
        case bac_a_sable, aire_de_jeux, manège
    }
}

struct GameCategory : Identifiable {
    let id = UUID()
    var title : String
    var image : String
    var games : [Game]
}

struct Game : Identifiable {
    let id = UUID()
    var title: String
    var imageName: String
}



/*struct GameCategory2 : Identifiable {
    let id = UUID()
    var title : String
    var descrription : String
    var image : String
}*/

struct GameScreen : Identifiable {
    let id = UUID()
    var time : Int
    var objectif : String
    var level : String //enum ?
    var description : String
}

struct Friend : Identifiable {
    let id = UUID()
    var name : String
    var image : String
    var online: Bool
    var coordinate: CLLocationCoordinate2D
    var activity : Activity
}

struct Activity: Identifiable {
    let id = UUID()
    var title : String
    var description : String
}

struct Prize : Identifiable {
    let id = UUID()
    var text : String
    var image : String
}

struct SettingCategory : Identifiable {
    let id = UUID()
    var title : String
    var modalPage : String
    var numberModal : Int
}



/*
 struct Article: Identifiable {
    let id = UUID()
    var title : String
    var description : String
    var date : Date
    var type : ArticleType
 }
 
 enum ArticleType {
    case type1, type 2, type 3
 */

//Obstacle
struct Obstacles: Identifiable {
    let id = UUID()
    var image: String
}
