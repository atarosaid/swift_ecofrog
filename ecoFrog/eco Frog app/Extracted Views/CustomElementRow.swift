//
//  CustomElementRow.swift
//  eco Frog app
//
//  Created by Said Ataro on 08/12/2020.
//

import SwiftUI

struct CustomElementRow: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct CustomElementRow_Previews: PreviewProvider {
    static var previews: some View {
        CustomElementRow()
    }
}
