//
//  CustonButton.swift
//  eco Frog app
//
//  Created by Said Ataro on 08/12/2020.
//

import SwiftUI

struct CustonButton: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct CustonButton_Previews: PreviewProvider {
    static var previews: some View {
        CustonButton()
    }
}
