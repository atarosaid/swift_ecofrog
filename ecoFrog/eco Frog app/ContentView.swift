//
//  ContentView.swift
//  eco Frog app
//
//  Created by Said Ataro on 08/12/2020.
//

import SwiftUI

struct Parametre: View {
    var body: some View {
        Image(systemName: "gearshape")
            .resizable()
            .frame(width: 100.0, height: 100.0)
    }
}



struct ContentView: View {
    var body: some View {
        VStack {
            TabView {
                Screen2()
                    .tabItem {
                        Image(systemName: "gamecontroller")
                        Text("Games")
                    }
                Recompense()
                    .tabItem {
                        Image(systemName: "hands.sparkles")
                        Text("Prizes")
                    }
                Amis()
                    .tabItem {
                        Image(systemName: "person.3.fill")
                        Text("Friends")
                    }
 
                Screen4()
                    .tabItem {
                        Image(systemName: "safari")
                        Text("Map")
                    }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
