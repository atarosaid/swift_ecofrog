//
//  FriendActivityDetail.swift
//  eco Frog app
//
//  Created by Said Ataro on 14/12/2020.
//

import SwiftUI

struct FriendActivityDetail: View {
    @Environment(\.presentationMode) private var  presentationMode 
    var friend : Friend
    var body: some View {
        
        VStack {
        Group {
            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark.circle.fill")
                    .resizable()
                    .frame(width: 40, height: 40)
                    .padding()
                Spacer()
            }
        Spacer()
        VStack {
            Spacer()
                Text("\(friend.name) invites you to participate:").bold()
                    .padding()
                VStack (alignment: .leading){
                    Text(friend.activity.title).bold()
                        .padding(5)
                    Text(friend.activity.description)
                        .padding()
                    HStack {
                        Spacer( )
                        Text("I participate").bold().foregroundColor(.white)
                            .padding()
                            .background(Color.green).cornerRadius(15)
                        
                        Spacer( )
                    }
                }.padding().foregroundColor(.purple)
            Spacer()
            }
        }
        }
    }
    
    struct FriendActivityDetail_Previews: PreviewProvider {
        static var previews: some View {
            FriendActivityDetail(friend: friend01)
        }
    }
} 
