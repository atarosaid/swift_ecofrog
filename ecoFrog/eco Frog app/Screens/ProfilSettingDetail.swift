//
//  ProfilSettingDetail.swift
//  eco Frog app
//
//  Created by Ayane Shoji on 15/12/2020.
//
import SwiftUI

struct ProfilSettingDetail: View {
    @Environment(\.presentationMode) private var presentationMode
    @State private var username: String = ""
    
    @State private var birthday = Date()
    
    var body: some View {
        ZStack{
            Color("frogColor").edgesIgnoringSafeArea(.all)
        
        VStack {
            Group {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image(systemName: "xmark.circle.fill")
                        .resizable()
                        .frame(width: 40, height: 40)
                        .padding()
                    Spacer()
                }
            }
            NavigationView {
                ZStack{
                    Color("frogColor").ignoresSafeArea()
                VStack(alignment:.leading) {
                    
                    Spacer()
                    HStack{
                        Text("Username")
                            .bold()
                        TextField("Modify username", text: $username)
                            .padding()
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                        
                    }
                    Spacer()
                    
                    Text("Enter your birthday")
                    DatePicker("Enter your birthday", selection: $birthday)
                        .datePickerStyle(GraphicalDatePickerStyle())
                        .frame(maxHeight: 400)
                }.padding()
                .navigationBarTitle("Profile setting")
            }
            
        }
    }
}
}
}
struct ProfilSettingDetail_Previews: PreviewProvider {
    static var previews: some View {
        ProfilSettingDetail()
    }
}
