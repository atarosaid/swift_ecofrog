import SwiftUI
struct CategoryRow: View {
    var category: GameCategory
    var body: some View {
        VStack {
            Image(category.image)
                .resizable()
                .cornerRadius(8)
                .frame(width: 100, height: 100, alignment: .center)
                .padding(7)
                .background(Color.black)
                .cornerRadius(12)
        }
    }
}

struct Screen2: View {
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
    ]
    
    var body: some View {
        
        VStack {
            NavigationView {
                ZStack{
                    Color("frogColor").ignoresSafeArea()
                    ScrollView{
                        VStack{
                            HStack {
                                
                                Spacer()
                                NavigationLink(destination: Setting(setting: setting01)){Image(systemName:"gearshape.fill")
                                    
                                    .resizable()
                                    .frame(width:50, height:50)
                                    .padding(5)
                                    .background(Color.white)
                                    .foregroundColor(.gray)
                                    .cornerRadius(100)
                                    .shadow(radius: 20 )
                                    .padding(5)
                                }
                            }
                            
                            Image("Eco Frog")
                                .resizable()
                                .frame(width:100, height:100)
                                .padding(5)
                                .background(Color.white)
                                .foregroundColor(.green)
                                .cornerRadius(100)
                                .shadow(radius: 20 )
                            Spacer()
                            
                            
                        }
                        
                        LazyVGrid(columns: columns, spacing: 50) {
                            ForEach(categories) {
                                category in
                                NavigationLink(
                                    destination: Screen3(category: category)) {
                                    CategoryRow(category:category)
                                }
                                
                            }
                        }
                    }
                }
                .navigationBarTitle(Text("Categories"))
            }
        }
        
    }
}


struct Screen2_Previews: PreviewProvider {
    static var previews: some View {
        Screen2()
    }
}
