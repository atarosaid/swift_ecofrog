//
//  Recompense.swift
//  eco Frog app
//
//  Created by MANUEL ADELE on 10/12/2020.
//

import SwiftUI

struct RecompenseRow: View {
    var recompense: Prize
    var body: some View {
        HStack{
            Image(recompense.image)
                .resizable()
                .cornerRadius(8)
                .blur(radius: 6)
                .frame(width: 100, height: 100, alignment: .center)
                .padding(5)
                .background(Color.black)
                .cornerRadius(12)
            
        }
    }
}

struct Recompense: View {
    
    let column = [
        GridItem(.flexible()),
        GridItem(.flexible()),
    ]
    var body: some View {
        
        VStack {
            NavigationView {
                ZStack {
                    Color("Color-3").edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    ScrollView(){
                        LazyVGrid(columns: column, spacing: 50) {
                            ForEach(récompenses) {
                                récompense in
                                
                                RecompenseRow(recompense: récompense )
                                
                            }
                        }
                    }
                }
                .navigationBarTitle(Text("Prizes"))
            }
        }
    }
    
}

struct Recompense_Previews: PreviewProvider {
    static var previews: some View {
        Recompense()
    }
}
