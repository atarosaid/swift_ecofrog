//
//  Game1.swift
//  eco Frog app
//
//  Created by Said Ataro on 14/12/2020.
//

import SwiftUI

struct Game1: View {
    @State private var heliPosition = CGPoint(x:40, y: 100)
    @State private var obstPosition = CGPoint(x:1000, y: 250)
    @State private var isPaused = false
    @State private var score = 0
    
    @State var timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        
        GeometryReader { geo in
            
            
            ZStack{
                
                FrogPlayer()
                    .position(self.heliPosition)
                    .onReceive(self.timer) {_ in
                        self.gravity()
                    }
                
                Obstacle()
                    .position(self.obstPosition)
                    .onReceive(self.timer) {_ in
                        self.obstMove()
                    }
                
                
                
                Text("Score : \(self.score)")
                    .foregroundColor(.white).bold()
                    .padding()
                    .background(Color.secondary)
                    .cornerRadius(5)
                    .position(x: geo.size.width - 100, y: geo.size.height / 5 )
                
                
                self.isPaused ? Button("Resume"){ self.resume() }.padding()
                    .background(Color.white).cornerRadius(10) : nil
                
                
            }
            .frame(width: geo.size.width, height: geo.size.height)
            .background(Color.black)
            .gesture(
                TapGesture()
                    .onEnded{
                        withAnimation{
                            self.heliPosition.y -= 100
                        }
                    })
            .onReceive(self.timer) { _ in
                self.collisionDetection();
                self.score += 1
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
    
    
    func gravity() {
        withAnimation{
            self.heliPosition.y += 20
        }
    }
    
    func obstMove() {
        if self.obstPosition.x > 0
        {
            withAnimation{
                self.obstPosition.x -= 20
            }
        }
        else
        {
            self.obstPosition.x = 1000
            self.obstPosition.y = CGFloat.random(in: 100...350)
            
        }
    }
    
    func pause() {
        self.timer.upstream.connect().cancel()
    }
    
    func resume() {
        
        self.timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
        
        self.obstPosition.x = 1000 // move obsitcle to starting position
        self.heliPosition = CGPoint(x: 100, y: 100) // helicopter to tarting position
        self.isPaused = false
        self.score = 0
    }
    
    func collisionDetection() {
        if abs(heliPosition.x + 50) > abs(obstPosition.x) && abs(heliPosition.y + 50) > abs(obstPosition.y) && abs(heliPosition.y) < abs(obstPosition.y) {
            self.pause()
            self.isPaused = true
        }
        //Collision bord
        if abs(heliPosition.y) < 0  {
            heliPosition.x = 10
        }
        if abs(heliPosition.y) > 500 {
            heliPosition.y = 0
        }
        
    }
    
    
    
}

struct Game1_Previews: PreviewProvider {
    static var previews: some View {
        Game1()
    }
}
