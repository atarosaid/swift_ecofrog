//
//  Screen4.swift
//  eco Frog app
//
//  Created by Ayane Shoji on 09/12/2020.


import SwiftUI
import MapKit

// GEOLOCALISATION
let lilleGeoloc = CLLocationCoordinate2D(
    latitude: 50.633333,
    longitude: 3.066667
)




struct Screen4: View {
    
    // 2 - Définir une région à afficher
    @State private var region = MKCoordinateRegion(
        
        // 2a - Définir le centre de la map à afficher
        center: lilleGeoloc,
        
        // 2b - Définir le rayon d'affichage
        span: MKCoordinateSpan(
            latitudeDelta: 0.15,
            longitudeDelta: 0.15
        )
    )
    @State var showingActivity = false
    
    @State var friendToShow: Friend = friend01
    
    var body: some View {
        VStack{
            NavigationView {
        // .4    - Mettre en place la map en renseignant : la région d'affichage et les points d'intérêts
                Map(coordinateRegion: $region, annotationItems: friends) { friend in
            
            // .5    - Afficher les points d'intérêt de type MapAnnotation
            MapAnnotation(coordinate: friend.coordinate ) {
                    VStack {
                    Button(action: {
                        friendToShow = friend
                        self.showingActivity.toggle()
                    })  {
                        Image(friend.image)
                            .resizable()
                            .cornerRadius(8)
                            .frame(width: 50, height: 50, alignment: .center)
                            .padding(5)
                            .background(Color.black)
                            .cornerRadius(12)
                        Text(friend.name).bold()
                    }
                }
            }
                
        }.sheet(isPresented: $showingActivity){
            FriendActivityDetail(friend:friendToShow)
               
        }.navigationBarTitle(Text("Map"))
    }
}
    }
}
struct Screen4_Previews: PreviewProvider {
    static var previews: some View {
        Screen4()
    }
}

