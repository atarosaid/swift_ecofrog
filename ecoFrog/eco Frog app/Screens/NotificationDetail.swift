//
//  NotificationDetail.swift
//  eco Frog app
//
//  Created by Ayane Shoji on 11/12/2020.
//

import SwiftUI

struct NotificationDetail: View {
    
    @Environment(\.presentationMode) private var presentationMode
    
    @State private var notificationAutorisation = true
    
    @State private var frequency = 0
    
    var body: some View {
        ZStack{
            Color("frogColor").edgesIgnoringSafeArea(.all)

        VStack {
            Spacer()
            Group {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image(systemName: "xmark.circle.fill")
                        .resizable()
                        .frame(width: 40, height: 40)
                        .padding()
                    Spacer()
                }
            }
        
            NavigationView {
                ZStack{
            Color("frogColor").edgesIgnoringSafeArea(.all)
                    VStack(alignment: .leading){
                        Toggle(isOn: $notificationAutorisation){
                            Text("Receive notifications")
                        }.padding()
                        
                        Picker(selection: $frequency, label: Text("Notification frequency")) {
                            Text("Daily").tag(0)
                            Text("Weekly").tag(1)
                            Text("Monthly").tag(2)
                        }.pickerStyle(SegmentedPickerStyle())
                        
                        
                    }.padding(30).navigationBarTitle(Text("Notification"))
                }
            }
        }
    }

}
}


struct NotificationDetail_Previews: PreviewProvider {
    static var previews: some View {
        NotificationDetail()
    }
}
