//
//  Setting.swift
//  eco Frog app
//
//  Created by Ayane Shoji on 11/12/2020.
//



import SwiftUI

struct SettingCategoryRow : View {
    var setting : SettingCategory
    
    @Binding var showModal: Bool
    
    @Binding var modalNumber : Int
    
    @Environment(\.presentationMode) var presentationMode
    
    
    
    var body: some View {
        
            Text(setting.title)
                .frame(width:100, height:100)
                .padding(5)
                .background(Color.white)
                .foregroundColor(.black)
                .cornerRadius(100)
                .shadow(radius: 20 )
                .onTapGesture {
                    modalNumber = setting.numberModal
                    showModal = true
                }

        }
    }



struct Setting: View {
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
    ]
    var setting : SettingCategory
    
    @State var showModal1 = false
   // @State var showModal2 = false
    //@State var showModal3 = false
    //@State var numberModal : Int
    
    @State var displayedModal : Int = 1
    
    var body: some View {
        
        VStack {
            //NavigationView {
                ZStack{
                    Color("frogColor").ignoresSafeArea()
                    ScrollView{
                        
                        LazyVGrid(columns: columns, spacing: 50) {
                            ForEach(settings){
                                setting in
                                SettingCategoryRow(setting:setting, showModal: $showModal1, modalNumber: $displayedModal)
                            }
                            
                        }
                    }
                }.navigationBarTitle(Text("Setting"))
            //}
        } .sheet(isPresented: self.$showModal1){
            if displayedModal == 1 {
                NotificationDetail()
            }
            else if displayedModal == 2 {
                ProfilSettingDetail()
            }
            else if displayedModal == 3 {
                ParentalControlDetail()
            }
            else if displayedModal == 4 {
                LanguageDetail()
            }
            else if displayedModal == 5 {
                ConnectionDetail()
            }
            else {
                AboutDetail()
            }
        }
        
    }
}

struct Setting_Previews: PreviewProvider {
    static var previews: some View {
        Setting(setting: setting01)
    }
}



