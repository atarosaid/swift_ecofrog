//
//  Screen1.swift
//  eco Frog app
//
//  Created by Said Ataro on 08/12/2020.
//

import SwiftUI

struct Screen1: View {
    var body: some View {
        VStack {
            Image("Eco Frog").resizable()
                .foregroundColor(.green)
                .aspectRatio(contentMode: .fit)
                .frame(width:200)
                
            Text("Eco Frog").font(.largeTitle).fontWeight(.bold).foregroundColor(Color.green)
        
        }
    }
}

struct Screen1_Previews: PreviewProvider {
    static var previews: some View {
        Screen1()
    }
}
