//
//  ParentalControlDetail.swift
//  eco Frog app
//
//  Created by Ayane Shoji on 15/12/2020.
//

import SwiftUI

struct ParentalControlDetail: View {
    @Environment(\.presentationMode) private var presentationMode
    var body: some View {
        ZStack{
            Color("frogColor").edgesIgnoringSafeArea(.all)
        VStack {
            Group {
            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark.circle.fill")
                    .resizable()
                    .frame(width: 40, height: 40)
                    .padding()
                Spacer()
            }
        }
        
            NavigationView{
                ZStack{
                    Color("frogColor").ignoresSafeArea()
                VStack{
                    Text("Enter your parental control code").bold()
                    HStack{
                        Rectangle()
                            .fill(Color.white)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                            .frame(width: 50, height: 50, alignment: .leading)
                        
                        Rectangle()
                            .fill(Color.white)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                            .frame(width: 50, height: 50, alignment: .leading)
                        
                        Rectangle()
                            .fill(Color.white)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                            .frame(width: 50, height: 50, alignment: .leading)
                        Rectangle()
                            .fill(Color.white)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                            .frame(width: 50, height: 50, alignment: .leading)
                        
                    }
                    
                }.navigationBarTitle("Parental control")
            }
            }
        }
    }
}
}
struct ParentalControlDetail_Previews: PreviewProvider {
    static var previews: some View {
        ParentalControlDetail()
    }
}
