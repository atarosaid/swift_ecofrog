//
//  ConnectionDetail.swift
//  eco Frog app
//
//  Created by Ayane Shoji on 15/12/2020.
//

import SwiftUI

struct ConnectionDetail: View {
    @Environment(\.presentationMode) private var presentationMode
    @State private var connectionAutorisation = true
    
    
    var body: some View {
        ZStack{
            Color("frogColor").edgesIgnoringSafeArea(.all)
        VStack {
            Group {
            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark.circle.fill")
                    .resizable()
                    .frame(width: 40, height: 40)
                    .padding()
                Spacer()
            }
        }
            NavigationView {
                ZStack{
                    Color("frogColor").ignoresSafeArea()
                    VStack(alignment: .leading)
                    {
                        
                        Toggle(isOn: $connectionAutorisation){
                            Text("Connection to network").bold()
                        }.padding()
                        
                        
                        
                    }.padding(30).navigationBarTitle(Text("Connection"))
                }
            }
        }
    }
    
}
}

struct ConnectionDetail_Previews: PreviewProvider {
    static var previews: some View {
        ConnectionDetail()
    }
}
