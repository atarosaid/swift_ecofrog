//
//  Language.swift
//  eco Frog app
//
//  Created by Ayane Shoji on 15/12/2020.
//
import SwiftUI



struct LanguageDetail: View {
    @Environment(\.presentationMode) private var presentationMode
    var body: some View {
        VStack{
            Group {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image(systemName: "xmark.circle.fill")
                        .resizable()
                        .frame(width: 40, height: 40)
                        .padding()
                    Spacer()
                }
            }
            NavigationView {
               

                List {
                    
                    Text("English")
                    Text("Français")
                    Text("Español")
                    Text("日本語")
                    Text("العربية")
                    Text("简体中文")
                    Text("हिंदी")
                    
                }.navigationBarTitle("Language")
                .padding()
            }
        }
    }
}

struct LanguageDetail_Previews: PreviewProvider {
    static var previews: some View {
        LanguageDetail()
    }
}
