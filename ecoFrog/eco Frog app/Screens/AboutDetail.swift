//
//  AboutDetail.swift
//  eco Frog app
//
//  Created by Ayane Shoji on 15/12/2020.
//

import SwiftUI

struct AboutDetail: View {
    @Environment(\.presentationMode) private var presentationMode
    var body: some View {
        ZStack{
            Color("frogColor").edgesIgnoringSafeArea(.all)
        VStack {
            Group {
            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark.circle.fill")
                    .resizable()
                    .frame(width: 40, height: 40)
                    .padding()
                Spacer()
            }
        }
            NavigationView {
                ZStack{
                    Color("frogColor").ignoresSafeArea()
                    VStack
                    {
                        Text("Copyright 2020 EarthiOS").bold()
                            .italic().padding()
                            
                        
                        Image("Eco Frog")
                            .resizable()
                            .frame(width:100, height:100)
                            .padding(5)
                            .background(Color.white)
                            .foregroundColor(.green)
                            .cornerRadius(100)
                            .shadow(radius: 20 )
                        Text("Eco Frog").bold()
                        .foregroundColor(.white)
                            .padding()
                            .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                        
                        
                        Text("Members:")
                            .bold()
                            .padding()
                        Text(friend01.name).bold()
                        Text(friend02.name).bold()
                        Text(friend03.name).bold()
                        Text(friend04.name).bold()
                        Text(friend05.name).bold()
                        
                    }.padding()
                    
                    
                    
               .padding(30).navigationBarTitle(Text("About"))
                }
            }
        }
    }
}
}
struct AboutDetail_Previews: PreviewProvider {
    static var previews: some View {
        AboutDetail()
    }
}
