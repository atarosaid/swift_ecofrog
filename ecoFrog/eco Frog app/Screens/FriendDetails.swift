//
//  FriendDetails.swift
//  eco Frog app
//
//  Created by MANUEL ADELE on 10/12/2020.
//
import SwiftUI
import MapKit

struct FriendDetails: View {
    var friend: Friend
    
    
    @State private var region = MKCoordinateRegion(
        
        // 2a - Définir le centre de la map à afficher
        center: friend01.coordinate,
        
        span: MKCoordinateSpan(
            latitudeDelta: 0.05,
            longitudeDelta: 0.05
        )
    )
    
    @State var showingDetail = false
        
        var body: some View {
            
            VStack {
                Map(coordinateRegion: $region, annotationItems: [friend] ) {
                        friend in
                        MapAnnotation(coordinate: friend.coordinate) {
                            VStack {
                                Button(action: {
                                    self.showingDetail.toggle()
                                }){
                                   Image(friend.image)
                                    .resizable()
                                    .cornerRadius(8)
                                    .frame(width: 50, height: 50, alignment: .center)
                                    .padding(5)
                                    .background(Color.black)
                                    .cornerRadius(12)
                                    
                                    
                                    Text(friend.name).bold()
                            }
                            }.sheet(isPresented: $showingDetail){
                                FriendActivityDetail(friend:friend)
                            }
                        }
                }.onAppear(){
                    region.center = friend.coordinate
                }
            }.ignoresSafeArea()
        }
    }



struct FriendDetails_Previews: PreviewProvider {
    static var previews: some View {
        FriendDetails(friend: friend01)
    }
}
