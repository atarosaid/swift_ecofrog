import SwiftUI

struct FriendsRow: View {
    var friend: Friend
    var body: some View {
        HStack(alignment: .center, spacing: nil, content: {
            
            Image(friend.image)
                .resizable()
                .cornerRadius(8)
                .frame(width: 100, height: 100, alignment: .center)
                .padding(5)
                .background(Color.black)
                .cornerRadius(12)
          
            Text(friend.name).bold()
            if friend.online {
                Circle().frame(width: 20, height: 20).foregroundColor(.green)
            } else {
                Circle().frame(width: 20, height: 20).foregroundColor(.red)
            }
            Spacer()
            
        })
        .padding()
        
    }
}

struct Amis: View {
    
    @State private var showingDetail = true //
  @Environment(\.presentationMode) var presentationMode  //
    //Filtrer les amis(es) par .online
    var filteredFriends = friends.sorted { (a,b) -> Bool in
        return a.online == true
    }
    //
    let column = [
        GridItem(.flexible()),]
    
    var body: some View {
           
     VStack {
            NavigationView {
                ZStack{
                    Color("friendsColor02").ignoresSafeArea()
                    ScrollView(){
                        LazyVGrid(columns: column, spacing: 1) {
                            ForEach(filteredFriends){ friend in
                                NavigationLink(destination:FriendDetails(friend: friend) ) {
                                    FriendsRow(friend: friend)
                                }
                            }
                        }
                    }
                }
                .navigationBarTitle(Text("Friends"))
            }
        }
    }
}

struct Amis_Previews: PreviewProvider {
    static var previews: some View {
        Amis()
    }
}
