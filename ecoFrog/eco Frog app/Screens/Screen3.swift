import SwiftUI

struct CategoryRow2: View {
    var game: Game
    var body: some View {
        HStack{
            Image(game.imageName)
                .resizable()
                .cornerRadius(8)
                .frame(width: 100, height: 100, alignment: .center)
                .padding(5)
                .background(Color.black)
                .cornerRadius(12)
        }
    }
}

struct Screen3: View {
    var category: GameCategory
    let column = [
        GridItem(.flexible()),
        GridItem(.flexible()),
    ]
    var body: some View {
        
        GeometryReader { geo in
                ZStack{
                    Color("Color-3").edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                ScrollView(){
                    LazyVGrid(columns: column, spacing: 50) {
                        ForEach(category.games) {
                            game in
                            NavigationLink(
                                destination: Game1()) {

                            CategoryRow2(game:game)

                            }

                        }
                    }
                }
                
            }.navigationBarTitle(Text("\(category.title)"))
        }
    }
}



struct Screen3_Previews: PreviewProvider {
    static var previews: some View {
        Screen3(category: supermarché)
        
    }
}

