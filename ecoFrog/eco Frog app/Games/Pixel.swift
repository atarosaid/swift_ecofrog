//
//  Pixel.swift
//  eco Frog app
//
//  Created by MANUEL ADELE on 15/12/2020.
//

import SwiftUI

struct Pixel: View {
    let size: CGFloat
    let color: Color
    
    
    var body: some View {
        Rectangle()
            .frame(width: size, height: size)
            .foregroundColor(color)
    }
    
    
}
