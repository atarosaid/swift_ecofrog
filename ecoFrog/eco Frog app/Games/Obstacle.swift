//
//  Obstacle.swift
//  eco Frog app
//
//  Created by MANUEL ADELE on 15/12/2020.
//

import SwiftUI

struct Obstacle: View {
    let width: CGFloat = 75
    let height: CGFloat = 50
    
    var body: some View {
        HStack{
        Image(obstacle01.image)
            .resizable()
            .frame(width: width, height: height)
            .foregroundColor(Color.green)
        }
        
    }
}

struct Obstacle_Previews: PreviewProvider {
    static var previews: some View {
        Obstacle()
    }
}
