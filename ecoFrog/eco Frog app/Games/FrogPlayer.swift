//
//  FrogPlayer.swift
//  eco Frog app
//
//  Created by MANUEL ADELE on 15/12/2020.
//

import SwiftUI

struct FrogPlayer: View {
    var body: some View {
        VStack (spacing: 0) {
            HStack (spacing: 0) {
                Image("frogPlayer")
                    .resizable()
                    .frame(width: 30, height: 30)
            }
        }
    }
}

struct FrogPlayer_Previews: PreviewProvider {
    static var previews: some View {
        FrogPlayer()
    }
}
