//
//  eco_Frog_appApp.swift
//  eco Frog app
//
//  Created by Said Ataro on 08/12/2020.
//

import SwiftUI

@main
struct eco_Frog_appApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
